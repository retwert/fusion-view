import request from '@/utils/request'

// 查询用户收藏echarts素材关联列表
export function listEcharts(query) {
  return request({
    url: '/datav/user/echarts/list',
    method: 'get',
    params: query
  })
}

// 查询用户收藏echarts素材关联详细
export function getEcharts(id) {
  return request({
    url: '/datav/user/echarts/' + id,
    method: 'get'
  })
}

// 新增用户收藏echarts素材关联
export function addEcharts(data) {
  return request({
    url: '/datav/user/echarts',
    method: 'post',
    data: data
  })
}

// 修改用户收藏echarts素材关联
export function updateEcharts(data) {
  return request({
    url: '/datav/user/echarts',
    method: 'put',
    data: data
  })
}

// 删除用户收藏echarts素材关联
export function delEcharts(id) {
  return request({
    url: '/datav/user/echarts/' + id,
    method: 'delete'
  })
}

// 导出用户收藏echarts素材关联
export function exportEcharts(query) {
  return request({
    url: '/datav/user/echarts/export',
    method: 'get',
    params: query
  })
}
// 删除用户收藏
export function delUserEcharts(data) {
  return request({
    url: '/datav/user/echarts/del',
    method: 'post',
    data: data
  })
}