# 圆点环形图

## 一、配置

### 1、图层

选中该环形图组件，在操作界面右侧的“图层名称”处修改该组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理；

### 2、标题

选中该环形图组件，在操作界面右侧的“标题”处可修改该组件的标题。

- 标题：标题显示的内容，如果不想显示标题，不填写内容就不显示；

- 副标题：副标题内容，如果不想显示副标题，不填写内容就不显示；

- 标题位置：单选，分为「居左」「居中」「居右」，如下图；

- 标题字号：标题文字的字体大小；

- 标题粗细：标题文字字体的粗细，可选：
  > * 'normal'：正常
  >
  > * 'bold'：粗体
  >
  > * 'bolder'：加粗体
  >
  > * 'lighter'：较轻

- 字体样式：标题文字的字体系列；

### 3、图例

选中该环形图组件，在操作界面右侧的“图例”处可修改该组件表盘有关的属性设置。

- 显示图例开关：是否显示图例；

- 图例方向：单选，图例方向有：「纵向」「横向」；

- 图例垂直位置：单选，分为「顶部」「中部」「底部」；

- 图例水平位置：单选，分为「居左」「居中」「居右」；

### 4、环柱

选中该环形图组件，在操作界面右侧的“环柱”处可修改该属性设置。

- 外扩半径：环形图向外扩张半径；
  <viewer>
    <img src="../image/circle-4-1.png" width="60%">
  </viewer>

- 内缩半径：环形图向内缩小半径；
  <viewer>
    <img src="../image/circle-4-2.png" width="60%">
  </viewer>

- 环柱宽度：彩色环柱的宽度；
  <viewer>
    <img src="../image/circle-4-3.png" width="60%">
  </viewer>

- 环柱最大值：环形单柱的最大值；

<!-- - 底圆点样式：单选，彩色圆环下方圆点样式； -->

- 圆点宽度：圆点的宽度；
  <viewer>
    <img src="../image/circle-4-4.png" width="60%">
  </viewer>

### 5、动画

选中该组件，在操作界面右侧的“动画”处可修改组件的载入动画效果。分别为：

- 弹跳

- 渐隐渐显

- 向右滑入

- 向左滑入

- 放缩

- 旋转

- 滚动

## 二、数据

静态数据格式：

```
  [
    {"value":14,"name":"暗渠"},
    {"value":32,"name":"暗管"},
    {"value":288,"name":"管控"},
    {"value":463,"name":"水渠"},
    {"value":763,"name":"集中式"}
  ]
```
