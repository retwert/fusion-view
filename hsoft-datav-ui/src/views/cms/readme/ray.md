# 射线

## 一、配置

- 图层名称：组件的名称。（建议设置，方便后期组件管理）

- 颜色：射线的颜色，例如设置为蓝色，如下图；

  <viewer>
  <img src="../image/ray-1.png"width="60%">
  </viewer>

- 动画时长：射线从起点扩散到终点的时长，以秒为单位；

- 是否反向开关：控制射线是否为反向扩散；

- 载入动画：组件的载入动画效果。分别为：弹跳、渐隐渐显、向右滑入、向左滑入、放缩、旋转、滚动。
