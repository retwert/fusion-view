import FileSaver from 'file-saver'

export function download(option,dynamicTags) {
   let pname = String(Math.random()).replace(".", "");
   let htmlStr = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"+
                    "<html xmlns=\"http://www.w3.org/1999/xhtml\" style=\"width: 95%; height: 95%; \">\n"+
                    "<head>\n"+
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"+
                    "<title>"+pname+"</title>\n"+
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\n";

      htmlStr += "</head><body style=\"width: 100%; height: 100%; \">\n";
      htmlStr += "<script type=\"text/javascript\" src=\"https://fastly.jsdelivr.net/npm/echarts@4.7.0/dist/echarts.min.js\"></script>\n";
      htmlStr += "<script type=\"text/javascript\" src=\"https://fastly.jsdelivr.net/npm/echarts/map/js/china.js\"></script>\n";
      htmlStr += "<script type=\"text/javascript\" src=\"https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js\"></script>\n";

      //动态加载外部js
      dynamicTags.forEach(element => {
         htmlStr += "<script type=\"text/javascript\" src=\""+element+"\"></script>\n";
      });


      htmlStr += "<div id=\"panel\" style=\"width: 100%; height: 100%; \"></div>\n";
      htmlStr += "<script type=\"text/javascript\">\n" +
        "\t var myChart = echarts.init(document.getElementById('panel'));\n" +
        "\t var option = " + JSON.stringify(option) + ";\n" +
        "\t myChart.setOption(option, true);" + "\n" +
        "</script>\n";
      htmlStr += "</body></html>";

      var blob = new Blob([htmlStr], {type: "text/plain;charset=utf-8"});//out_put_string为需要保存到文件的字符串内容

      FileSaver.saveAs(blob, pname+".html");

}