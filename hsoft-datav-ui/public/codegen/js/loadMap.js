function loadMap(mapCode, name, myChart, dataOption) {
	//console.log("QAQ", dataOption);
    $.get(mapCode, function(data) {
        if (data) {
            echarts.registerMap(name, data);
            var option = {
                //backgroundColor: '#101129',
                title: {
                    text: dataOption.text, //主标题
                    textStyle: {
                        color: '#fff', //颜色
                        //fontStyle: 'normal', //风格
                        //fontWeight: 'normal', //粗细
                        //fontFamily: '微软雅黑', //字体
                        //fontSize: 18, //大小
                        fontWeight: dataOption.fontWeight, //粗细
                        fontFamily: dataOption.fontFamily, //字体
                        fontSize: dataOption.fontSize, //大小
                        align: 'center' //水平对齐
                    },
                    subtext: dataOption.subtext, //副标题
                    subtextStyle: { //对应样式
                        color: '#fff',
                        fontSize: dataOption.fontSize - 5,
                        align: 'center'
                    },
                    x: dataOption.x,
                    itemGap: 7
                },
                tooltip: {
                    show: true,
                    formatter: function(params) {
                        if (params.data) return params.name + '：' + params.data['value']
                    },
                },
                visualMap: {
                    type: 'continuous',
                    text: ['', ''],
                    showLabel: true,
                    left: '50',
                    min: 0,
                    max: 100,
                    inRange: {
                        color: ['#edfbfb', '#b7d6f3', '#40a9ed', '#3598c1', '#215096']
                    },
                    splitNumber: 0
                },
                series: [{
                    name: 'MAP',
                    type: 'map',
                    mapType: name,
                    //zoom: 1, //当前视角的缩放比例
    				//roam: true, //是否开启平游或缩放
                    selectedMode: 'false', //是否允许选中多个区域
                    label: {
                        normal: {
                            show: true,
                            color: '#000'
                        },
                        emphasis: {
                            show: true,
                            color: '#000'
                        }
                    },
                    data: dataOption.staticDataValue
                }]
                
            };
            myChart.setOption(option);
        } else {
            alert('无法加载该地图');
        }
    });
}